import axios from 'axios';

const axiosInstance = axios.create({
  // baseURL: 'https://www.belstardoc.com/api'
  baseURL: 'http://127.0.0.1:8848/api'
});

export default axiosInstance
